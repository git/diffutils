# This file is sourced by init.sh, *before* its initialization.

# Copyright (C) 2010-2025 Free Software Foundation, Inc.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Skip the current test if valgrind doesn't work,
# which could happen if not installed,
# or hasn't support for the built architecture,
# or hasn't appropriate error suppressions installed etc.

# This goes hand in hand with the "exec 9>&2;" in tests/Makefile.am's
# TESTS_ENVIRONMENT definition.
stderr_fileno_=9

# Having an unsearchable directory in PATH causes execve to fail with EACCES
# when applied to an unresolvable program name, contrary to the desired ENOENT.
# Avoid the problem by rewriting PATH to exclude unsearchable directories.
# Also, if PATH lacks /sbin and/or /usr/sbin, append it/them.
sanitize_path_()
{
  # FIXME: remove double quotes around $IFS when all tests use init.sh.
  # They constitute a work-around for a bug in FreeBSD 8.1's /bin/sh.
  local saved_IFS="$IFS"
    IFS=$PATH_SEPARATOR
    set -- $PATH
  IFS=$saved_IFS

  local d d1
  local colon=
  local new_path=
  for d in "$@"; do
    test -z "$d" && d1=. || d1=$d
    if ls -d "$d1/." > /dev/null 2>&1; then
      new_path="$new_path$colon$d"
      colon=$PATH_SEPARATOR
    fi
  done

  for d in /sbin /usr/sbin ; do
    case "$PATH_SEPARATOR$new_path$PATH_SEPARATOR" in
      *$PATH_SEPARATOR$d$PATH_SEPARATOR*) ;;
      *) new_path="$new_path$PATH_SEPARATOR$d" ;;
    esac
  done

  PATH=$new_path
  export PATH
}

require_timeout_()
{
  ( timeout 10s true ) > /dev/null 2>&1 \
    || skip_ your system lacks the timeout program
  returns_ 1 timeout 10s false \
    || skip_ your system has a non-GNU timeout program
  returns_ 124 timeout 0.01 sleep 0.02 \
    || skip_ "'timeout 0.01 sleep 0.02' did not time out"
}

require_valgrind_()
{
  require_timeout_
  local errout; errout=$(
    LC_ALL=C timeout --signal=9 3 valgrind --error-exitcode=1 diff /dev/null /dev/null 2>&1
  ) ||
    skip_ "requires a working valgrind"
  case $errout in
    *'Serious error'*)
      skip_ "requires a valgrind without serious errors";;
  esac
}

# Skip the current test if we lack Perl.
require_perl_()
{
  : ${PERL=perl}
  $PERL -e 'use warnings' > /dev/null 2>&1 \
    || skip_ 'configure did not find a usable version of Perl'
}

# Run this test in a UTF-8 locale if possible, and skip the test otherwise.
# Prefer en_US for its diagnostics.
require_utf8_locale_()
{
  local locale

  if test "`(locale charmap) 2>/dev/null`" != UTF-8; then
    for locale in en_US.UTF-8 `(locale -a) 2>/dev/null` not-found; do
      case $locale in
        *.[Uu][Tt][Ff]*8)
	  if test "`(LC_ALL=$locale locale charmap) 2>/dev/null`" = UTF-8; then
	    LC_ALL=$locale
	    export LC_ALL
	    break
	  fi;;
        not-found)
	  skip_ "No UTF-8 locale found";;
      esac
    done
  fi

  # Solaris 10's /usr/bin/tr can silently malfunction. Try to find one that works.
  found_working_tr=0
  for i in tr /usr/xpg4/bin/tr gtr; do
    tr() { env $i "$@"; }
    test '一' = "$(printf 一|tr a b)" && { found_working_tr=1; break; }
  done
  test $found_working_tr = 1 || skip_ "failed to find a working tr program"
}

# Some systems lack seq.
# A limited replacement for seq: handle 1 or 2 args; increment must be 1
seq()
{
  case $# in
    1) start=1  final=$1;;
    2) start=$1 final=$2;;
    *) echo you lose 1>&2; exit 1;;
  esac
  awk 'BEGIN{for(i='$start';i<='$final';i++) print i}' < /dev/null
}

sanitize_path_
